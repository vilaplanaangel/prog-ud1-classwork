# PROG-UD1-CLASSWORK

### Exercisis de introducció a la programació
* [Activitat 1](activitats/activitat01.md) <br>
* [Activitat 2](activitats/activitat02.md) <br>
* [Activitat 3](activitats/activitat03.md) <br>
* [Activitat 4](activitats/activitat04.md) <br>
* [Activitat 5](activitats/activitat05.md) <br>
* [Activitat 6](activitats/activitat06.md) <br>
* [Activitat 7](activitats/activitat07.md) <br>
* [Activitat 8](activitats/activitat08.md) <br>
* [Activitat 9](activitats/activitat09.md) <br>
* [Activitat 10](activitats/activitat10.md) <br>
* [Activitat 11](activitats/activitat11.md) <br>
* [Activitat 12](activitats/activitat12.md) <br>
* [Activitat 13](activitats/activitat13.md) <br>
* [Activitat 14](activitats/activitat14.md) <br>
* [Activitat 15](activitats/activitat15.md)