# Activitat 3
Dissenya un algorisme que mostre quant valdran unes deportives amb un preu de
85.00 euros, si estan rebaixades un 15%.

## Pseudocodi
```
Variables
  preu decimal
  descompte decimal
  preuTotal decimal
inici
  preu ← 85.00
  descompte ← preu*0.15
  preuTotatl ← preu - descompte
  Escribir "El preu total és: " + preuTotal 
fi
```
## Diagrama de fluxe
![alt text](diagrames/diagrama03.png "Activitat 3")
