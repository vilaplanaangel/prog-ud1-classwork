# Activitat 1
Dissenya un algorisme que permeta llegir les dades de un usuari (nom i cognoms) y els mostre per la pantalla.

## Pseudocodi
```
Variables
  nom cadena
  cognoms cadena
inici
  Escriure "Introduïsca el teu nom: "
  Llegir nom
  Escriure "Introduïsca el teu cognom: "
  Llegir cognom
  Escriure "Nom: " + nom
  Escriure "Cognom: " + cognom
fi
```
## Diagrama de fluxe
![alt text](diagrames/diagrama01.png "Activitat 1")
