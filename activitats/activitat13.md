# Activitat 13
Dissenya un algorisme que demane 10 números a l'usuari, i per a cada número indique si és parell o imparell.

## Pseudocodi
```
Variables
  num enter
  contador enter
inici
  contador ← 0
  mentres (contador < 10)
    Escriure "Introduïsca un número: "
    Llegir num;
    si (num%2 == 0)
      Escriure "El número " + num + " es parell"
    si no
      Escriure "El número " + num + " es imparell"
    fi_si
    contador++
  fi_mentres
fi
```
## Diagrama de fluxe
![alt text](diagrames/diagrama13.png "Activitat 13")
