# Activitat 11
Escriu un programa que faça la sumatòria dels nombres enters compresos entre l'1 i el 10, és a dir, 1 + 2 + 3 + .... + 10.

## Pseudocodi
**Aplicant Fórmula de Gauss**
```
Variables
  primer enter
  final enter
  longitud enter
  sum enter
inici
  primer ← 1
  final ← 10
  longitud ← 10
  sum ← ((primer+final)*longitud)/2
  Escriure "El resultat final és: " + sum 
fi
```

<br>

**Bucle**
```
Variables
  contador enter
  sum enter
inici
  sum ← 0
  contador ← 1
  mentres (contador <= 10)
    sum+=contador
    contador++
  fi_mentres
  Escriure "El resultat final és: " + sum
fi
```
## Diagrama de fluxe
![alt text](diagrames/diagrama11.png "Activitat 11")
