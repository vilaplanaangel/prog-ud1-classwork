# Activitat 7
Dissenya un algorisme que demane a l'usuari 3 números. Si el primer és positiu, ha de calcular el producte dels altres dos. En cas contrari, ha de calcular la suma. Mostrar en els dos casos el resultat.

## Pseudocodi
```
Variables
  num1 enter 
  num2 enter
  num3 enter
  calcul enter
inici
  Escriure "Introduïsca el primer número: "
  Llegir num1
  Escriure "Introduïsca el segon número: "
  Llegir num2
  Escriure "Introduïsca el tercer número: "
  Llegir num3

  si (num1 >= 0)
     calcul ← num1*num2*num3
     Escriure "El producte total es: " + calcul
  Si no
    calcul ← num1+num2+num3
    Escriure "La suma total es " + calcul
  fi_si
fi
```
## Diagrama de fluxe
![alt text](diagrames/diagrama07.png "Activitat 7")
