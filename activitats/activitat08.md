# Activitat 8
Dissenya un algoritme que pregunte al usuari un número del 1 al 10 i imprimixca per pantalla la taula de multiplicar corresponent.

## Pseudocodi
```
Variables
  num enter
  contador enter
inici
  Escriure "Introduïsca un número del 1 al 10: "
  Llegir num
  mentres (num<1 o num>10)
    Escriure "Valor no vàlid. Introduïsca un nou valor: "
    Llegir num
  fi_mentres
  contador ← 1
  mentres (contador < 11)
    Escriure num + " x " + contador + " = " + num*contador
    contador++
  fi_mentres
fi
```
## Diagrama de fluxe
![alt text](diagrames/diagrama08.png "Activitat 8")
