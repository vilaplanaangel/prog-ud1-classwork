# Activitat 6
Dissenya un algorisme que demane a l'usuari el número de graus Celsius i indique l'equivalent en Fahrenheit.

## Pseudocodi
```
Variables
  celsius decimal
  fahrenheit decimal
inici
  Escriure "Introduïsca el valor en celsius: "
  Llegir celsius
  fahrenheit ← (9/5)celsius+32
  Escriure "Celsius: "+celsius
  Escriure "Fahrenheit: "+fahrenheit
fi
```
## Diagrama de fluxe
![alt text](diagrames/diagrama06.png "Activitat 6")
