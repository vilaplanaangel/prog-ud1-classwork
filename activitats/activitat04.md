# Activitat 4
Dissenya un algorisme que visualitze en pantalla quants diners li donarà el banc a un client després de 6 mesos si posa 2000€ en un compte que li dona el 2,75% d'interès anual. Recorda que al pagar-te els interessos, el banc aplicarà una retenció del 18% per a hisenda.

## Pseudocodi
```
Variables
  diners decimal
  mesos enter
  interesBrut decimal
  retencio decimal
  interesNet decimal
inici
  diners ← 2000
  anys ← 0.5
  interesBrut ← (2.75/100)*diners*anys
  retencio ← interesBrut*0.18
  interesNet ← interesosBrut - retencio
  Escriure "El interes net és: " + interesNet
fi
```
## Diagrama de fluxe
![alt text](diagrames/diagrama04.png "Activitat 4")
