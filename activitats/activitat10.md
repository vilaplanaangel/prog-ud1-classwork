# Activitat 10
Dissenya un algoritme que demane tres valors els guarde en variables i ens mostre quin és el major i el més xicotet. Recorda veure que els tres valors siguen diferentes. En eixe cas haurà d'informar al usuari.

## Pseudocodi
```
Variables
  num1  enter
  num2  enter
  num3  enter
  major enter
  menor enter
inici
  Escriure "Introduïsca el primer número: "
  Llegir num1
  Escriure "Introduïsca el segon número: "
  Llegir num2
  mentres (num2 == num1)
    Escriure "Introduïsca un número diferent al primer: "
    Llegir num2
  fi_mentres
  si (num1 > num2)
    major ← num1
    menor ← num2
  si no
    major ← num2
    menor ← num1
  fi_si
  Escriure "Introduïsca el tercer número: "
  Llegir num3
  mentres (num3 == num1 o num3 == num2)
    Escriurer "Número repetit. Inroduïsca un altre valor: "
    Llegir num3
  fi_mentres
  si (num3 > major)
    major ← num3
  si no si (num3 < menor)
    menor ← num3
  fi_si
  Escriure "El número més major es: " + major
  Escriure "El número més menut es: " + menor
fi

```
## Diagrama de fluxe
![alt text](diagrames/diagrama10.png "Activitat 10")
