# Activitat 15
Dissenya un algorisme que demane 20 números a l'usuari, i mostre al final la quantitat de parells i la quantitat d'imparells que s'han introduït.

## Pseudocodi
```
Variables
  num enter
  cont enter
  par enter
  impar enter
inici
  cont ← 0
  par ← 0
  impar ← 0
  mentres (cont < 20)
    Escriure "Introduïsca un número"
    Llegir num
    si (num%2 == 0)
      par++
    si no
      impar--
    fi_si
    cont++
  fi_mentres
  Escriure "Números parells: " + par
  Escriure "Números imparells: " + impar
fi
```
## Diagrama de fluxe
![alt text](diagrames/diagrama15.png "Activitat 15")
