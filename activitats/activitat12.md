# Activitat 12
Escriu un programa que sol·licite a l'usuari una quantitat de segons i la convertisca en dies, hores, minuts i segons. Visualitza el resultat per pantalla.

## Pseudocodi
```
Variables
  temps enter
  segons enter
  minuts enter
  hores enter
  dies enter
inici
  Escriure "Introduïsca els segons: "
  Llegir temps
  dies ← temps/86400
  temps ← temps%86400
  hores ← temps/3600
  temps ← temps%3600
  minuts ← temps/60
  segons ← temps%60
  Escriure "Dies: " + dies
  Escriure "Hores: " + hores
  Escriure "Minuts: " + minuts
  Escriure "Segons: " + segons
fi
```
## Diagrama de fluxe
![alt text](diagrames/diagrama12.png "Activitat 12")
