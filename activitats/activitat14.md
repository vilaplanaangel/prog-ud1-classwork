# Activitat 14
Dissenya(algorisme) d'un programa que determine si un any és de traspàs (bisiesto en castellà). Un any és de traspàs si és múltiple de 4 (per exemple, 1984). No obstant això, els anys múltiples de 100 sols són de traspàs quan també són al mateix temps múltiples de 400 (per exemple, 1800 no és de traspàs, però 2000 sí).

## Pseudocodi
```
Variables
  any enter
inici
  Escriure "Introduïsca l'any: "
  Llegir any
  si (any%4 == 0)
    si (any%100 == 0)
      si (any%400 == 0)
        Escriure any + " és un any de traspàs"
      si no
        Escriure any + " no és un any de traspàs"
    si no
      Escriure any + " és un any de traspàs"
  si no
    Escriure any + " no és un any de traspàs"
  fi_si 
fi
```
## Diagrama de fluxe
![alt text](diagrames/diagrama14.png "Activitat 14")

