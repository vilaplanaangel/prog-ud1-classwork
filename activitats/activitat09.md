# Activitat 9
Dissenya un algoritme que visualitze una sèrie de números introduïts per teclat. El programa finalitzarà en quan l'usuari introduïsca un -1.

## Pseudocodi
```
Variables
  num enter
inici
  Escriure "Introduïsca un número. Per a acabar introduïsca -1: "
  Llegir num
  mentres (num != -1)
    Escriure "Introduïsca un altre número: "
    Llegir num
  fi_mentres
  Escriure "Fi del programa"
fi
```
## Diagrama de fluxe
![alt text](diagrames/diagrama09.png "Activitat 9")
