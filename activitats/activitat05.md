# Activitat 5
Dissenya un algorisme que demane a l'usuari dos números, els guarde en dos variables i intercanvie els valors. Ha de mostrar els valors intercanviats.

## Pseudocodi
```
Variables
  num1 enter
  num2 enter
  aux  enter
inici
  Escriure "Introduïsca un valor: "
  Llegir num1
  Escriure "Introduïsca el segon valor: "
  Llegir num2
  aux ← num1
  num1 ← num2
  num2 ← aux
  Escriure "Els valors s’han intercambiat!! :o" 
fi
```
## Diagrama de fluxe
![alt text](diagrames/diagrama05.png "Activitat 5")
