# Activitat 2
Dissenya un algorisme que calcule el àrea de quadrat i el mostre per pantalla.

## Pseudocodi
```
Variables
  costat decimal
  area decimal
inici
  Escriure "Introduïsca el costat del quadrat: "
  Llegir costat
  mentres (costat <= 0)
    Escriure "Error. Introduïsca un valor vàlid: "
    Llegir costat
  fi_mentres
  area = costat * costat
  Escriure "L'àrea és: " + area 
final
```
## Diagrama de fluxe
![alt text](diagrames/diagrama02.png "Activitat 2")
